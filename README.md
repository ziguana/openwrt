# OpenWrt for Traverse LS1088 Family / Ten64
## THIS BRANCH IS NOT IN USE

We periodically rebase (rather than merge) onto the latest OpenWrt tree.

Please see [the branches page](https://gitlab.com/traversetech/ls1088firmware/openwrt/-/branches) for the
latest development branch.

The current "recommended" branch is also published on the [Traverse Archive Server](https://archive.traverse.com.au/)
homepage.
